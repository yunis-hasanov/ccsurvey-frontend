$(function () {
    //console.log('Page LOADED!');
    $.ajax({
        method: 'GET',
        url: 'http://192.168.111.64:8080/api/campaigns',
        success: function (response) {
            //alert('Request has been sent to Server');
            console.log(response);
            var myObj, i, j, x = "";
            myObj = response;

            //console.log(myObj);
            if (myObj.code == 200) {
                for (i in myObj.data) {
                    //console.log(myObj.data[i]);
                    $('#cb_campaigns').append("<option value='" + myObj.data[i].id + "'>" + myObj.data[i].campaignName + "</option>");
                }
            } else {
                alert('Response-Code:' + myObj.code + ', Message:' + myObj.message);
            }
        },
        error: function (response) {
            alert('Error:' + response.message);
            console.log(response);
        },
    });

    $('#cb_campaigns').on('change', function () {
        //alert('Error:' + $('#cb_campaigns').val());
        $.ajax({
            method: 'GET',
            url: 'http://192.168.111.64:8080/api/campaigns/' + $('#cb_campaigns').val(),
            success: function (response) {
                //console.log(response);
                // $('#empty-field-validate').text("13121campaignName");
                //$('#Campaign_details').show();
                

                var myObj, i, j, x = "";
                myObj = response;

                //console.log(myObj);
                if (myObj.code == 200) {
                    loadQuestions($('#cb_campaigns').val());
                    getCampaignsMap($('#cb_campaigns').val());
                    $('#Campaigndetails').show();
                    $('#Questiondetails').hide();
                    $('#Questions').show();
                    $('#CampId').val(myObj.data.id);
                    $('#CampaignName').val(myObj.data.campaignName);
                    $('#Cdate').val(myObj.data.cdate);

                    var mySwitch = new Switchery($('#IsEnabled')[0], { size: "small", color: '#0D74E9' });

                    if (myObj.data.isEnabled == 1) {
                        setSwitchery(mySwitch, true);
                        // $('#IsEnabled').click();
                        // $('#IsEnabled').attr('checked', true);
                    }
                    else {
                        setSwitchery(mySwitch, false);
                        // $('#IsEnabled').attr('checked', false).checkboxradio("refresh");
                    }

                } else {
                    $('#Campaigndetails').hide();
                    $('#Questions').hide();
                    $('#Questiondetails').hide();
                    alert('Response-Code:' + myObj.code + ', Message:' + myObj.message);
                }
            },
            error: function (response) {
                alert('Error:' + response.message);
                console.log(response);
            },
        });
    });


    $('#cb_questions').on('change', function () {
        //alert('Error:' + $('#cb_campaigns').val());
        $.ajax({
            method: 'GET',
            url: 'http://192.168.111.64:8080/api//campaigns/' + $('#cb_campaigns').val() + '/questions/' + $('#cb_questions').val(),
            success: function (response) {
    
                var myObj, i, j, x = "";
                myObj = response;
    
                //console.log(myObj);
                if (myObj.code == 200) {
                    $('#Questiondetails').show();
                    $('#QID').val(myObj.data.id);
                    $('#QTitle').val(myObj.data.title);
                    $('#QCdate').val(myObj.data.cdate);
                    $('#QOrder').val(myObj.data.q_Order);
                    $('#QText_az').val(myObj.data.text_az);
                    $('#QText_en').val(myObj.data.text_en);
                    $('#QText_ru').val(myObj.data.text_ru);
                    $('#txtNote').val(myObj.data.note);
                    loadAnswers(myObj.data.id);
                    var mySwitch = new Switchery($('#QIsEnabled')[0], { size: "small", color: '#0D74E9' });
    
                    if (myObj.data.isEnabled == 1) {
                        setSwitchery(mySwitch, true);
                    }
                    else {
                        setSwitchery(mySwitch, false);
                    }
    
                } else {
                    $('#Questiondetails').hide();
                    alert('Response-Code:' + myObj.code + ', Message:' + myObj.message);
                }
            },
            error: function (response) {
                alert('Error:' + response.message);
                console.log(response);
            },
        });
    });
    

});



function setSwitchery(switchElement, checkedBool) {
    if ((checkedBool && !switchElement.isChecked()) || (!checkedBool && switchElement.isChecked())) {
        switchElement.setPosition(true);
        switchElement.handleOnchange(true);
    }
}

function loadQuestions(CampId) {
    // console.log('loadQuestions:' + CampId);
    $('#cb_questions').empty();
    $('#cb_questions').append('<option value="0" selected="selected" disabled>Select...</option>');
    $.ajax({
        method: 'GET',
        url: 'http://192.168.111.64:8080/api/campaigns/' + CampId + '/questions',
        success: function (response) {
            //alert('Request has been sent to Server');
            console.log(response);
            var myObj, i, j, x = "";
            myObj = response;

            //console.log(myObj);
            if (myObj.code == 200) {
                for (i in myObj.data) {
                    //console.log(myObj.data[i]);
                    $('#cb_questions').append("<option value='" + myObj.data[i].id + "'>" + myObj.data[i].title + "</option>");
                }
            } else {
                alert('Response-Code:' + myObj.code + ', Message:' + myObj.message);
            }
        },
        error: function (response) {
            alert('Error:' + response.message);
            console.log(response);
        },
    });
}


function loadAnswers(QID) {
    // console.log('loadAnswers:' + QID);
    $('[id^=answer_]').each(function (index, element) {
        removeElement(this.id.substring(7))
    });




    $.ajax({
        method: 'GET',
        url: 'http://192.168.111.64:8080/api/questions/' + QID + '/answers',
        success: function (response) {
            console.log(response);
            var answerCounter = 0;
            var myObj, i, j, x = "";
            myObj = response;

            //console.log(myObj);
            if (myObj.code == 200) {
                for (i in myObj.data) {
                    //console.log(myObj.data[i]);
                    //$('#cb_questions').append("<option value='" + myObj.data[i].id + "'>" + myObj.data[i].title + "</option>");
                    answerCounter++;
                    $( "#panel_Answers" ).append('<div id="answer_' + answerCounter + '" class="col-md-12" style="border: 2px solid #ddd;	border-radius: 5px; margin-bottom: 20px; margin-top: 10px; padding-bottom: 10px;">       \
                        <br/><ul style="float: right;" class="fab-menu fab-menu-top" data-fab-toggle="click" style="z-index: 1002;"><li><a class="fab-menu-btn btn btn-danger btn-float btn-rounded btn-icon" onClick = "removeElement(' + answerCounter + ');"><i class="fab-icon-open icon-minus3"></i><i class="fab-icon-close icon-cross2"></i></a></li></ul> \
												<div class="col-md-12">                                                       \
												    <input type="hidden" id="AID_' + answerCounter + '" value="' + myObj.data[i].id + '"  /> \
													<input type="text" class="form-control" id="ATitle_' + answerCounter + '"  value="' + myObj.data[i].title + '">        \
													<div class="label-block">                                                 \
														<span class="label label-primary">Answer title</span>                 \
													</div>                                                                    \
												</div> 											                              \
												<div class="col-md-12">                                                       \
													<input type="text" class="form-control" id="AText_az_' + answerCounter + '"  value="' + myObj.data[i].text_az + '">    \
													<div class="label-block">                                                 \
														<span class="label label-primary">Answer text (AZ)</span>             \
													</div>                                                                    \
												</div>                                                                        \
												<div class="col-md-12">                                                       \
													<input type="text" class="form-control" id="AText_en_' + answerCounter + '"  value="' + myObj.data[i].text_en + '">    \
													<div class="label-block">                                                 \
														<span class="label label-primary">Answer text (EN)</span>             \
													</div>                                                                    \
												</div>                                                                        \
												<div class="col-md-12">                                                       \
													<input type="text" class="form-control" id="AText_ru_' + answerCounter + '" value="' + myObj.data[i].text_ru + '">    \
													<div class="label-block">                                                 \
														<span class="label label-primary">Answer text (RU)</span>             \
													</div>                                                                    \
												</div>                                                                        \
												<div class="col-md-2">                                                       \
													<input type="number"  min="0" max="100" class="form-control" id="AOrder_' + answerCounter + '"  value="' + myObj.data[i].q_Order + '">        \
													<div class="label-block">                                                 \
														<span class="label label-primary">Order</span>                        \
													</div>                                                                    \
												</div>                                                                        \
												<div class="col-md-2">                                                       \
												<input type="checkbox" ' + (myObj.data[i].isEnabled == 1?'checked':'') + ' class="switchery-primary" id="AIsEnabled_' + answerCounter + '" >\
													<div class="label-block">                                                 \
														<span class="label label-primary">Is Enabled</span>                   \
													</div>                                                                    \
                                                </div>                                                                        \
                                                <div class="col-md-5">                                                        \
                                                <div class="input-group">                                                     \
                                                    <input  type="hidden"  id="ANextQID_' + answerCounter + '" value="' + myObj.data[i].nextQID + '" >        \
                                                    <input  type="text" readonly="readonly" class="form-control" id="ANextQTitle_' + answerCounter + '" value="' + myObj.data[i].nextQtext + '" >        \
                                                    <span class="input-group-btn">                                            \
                                                        <button class="btn btn-default" onClick = "loadNextQuestions(' + answerCounter + ');" type="button">Select</button>         \
                                                    </span>                                                                   \
                                                </div>                                                                        \
                                                <div class="label-block">                                                     \
                                                    <span class="label label-primary">Next Question</span>                    \
                                                </div>                                                                        \
                                            </div>                                                                            \
                        </div>');             
                }
            } else {
                alert('Response-Code:' + myObj.code + ', Message:' + myObj.message);
            }
        },
        error: function (response) {
            alert('Error:' + response.message);
            console.log(response);
        },
    });
}

function getCampaignsMap(CampId){
    $.ajax({
        method: 'GET',
        url: 'http://192.168.111.64:8080/api/campaigns/' + CampId + '/map',
        success: function (response) {
            //alert('Request has been sent to Server');
            console.log(response);
            var myObj, i, j, x = "";
            myObj = response;

            //console.log(myObj);
            if (myObj.code == 200) {
                loadCampMapUI(JSON.stringify(myObj.data));
            } else {
                alert('Response-Code:' + myObj.code + ', Message:' + myObj.message);
            }
        },
        error: function (response) {
            alert('Error:' + response.message);
            console.log(response);
        },
    });
}

