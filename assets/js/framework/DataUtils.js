$(function () {

	$('#submit_Campaign').click(function () {
		// click
		//console.log("Clicked..." + $('#IsEnabled').val());
		var Campame = $('#CampaignName').val().trim();
		if (Campame.length == 0) {
			ErrorAlert("Please fill the required fiedls!");
			return;
		}

		var obj = {
			ID: $('#CampId').val(),
			CampaignName: Campame,
			IsEnabled: ($('#IsEnabled').is(":checked")) ? 1 : 0
		};

		$.ajax({
			method: 'PUT',
			url: 'http://192.168.111.64:8080/api/campaigns',
			dataType: 'json',
			beforeSend: function (xhr) {
				xhr.setRequestHeader('Content-Type', 'application/json');
			},
			data: JSON.stringify(obj),
			success: function (response) {
				SuccessAlert();
				console.log(response);
				//$('#put_res').text(response.message);
			},
			error: function (response) {
				//alert('This is Error message!');
				ErrorAlert("");
				console.log(response);
			},
		});
	});


	$('#submit_Question').click(function () {
		// click
		if ($('#QTitle').val().trim().length == 0 || $('#CampId').val().trim().length == 0) {
			ErrorAlert("Please fill the required fiedls!");
			return;
		}

		var obj = {
			ID: $('#QID').val(),
			CampID: $('#CampId').val(),
			Title: $('#QTitle').val(),
			Q_Order: $('#QOrder').val(),
			Text_az: $('#QText_az').val(),
			Text_en: $('#QText_en').val(),
			Text_ru: $('#QText_ru').val(),
			IsEnabled: ($('#QIsEnabled').is(":checked")) ? 1 : 0,
			Note: $('#txtNote').val()
		};

		$.ajax({
			method: 'PUT',
			url: 'http://192.168.111.64:8080/api/campaigns/' + $('#CampId').val() + '/questions',
			dataType: 'json',
			beforeSend: function (xhr) {
				xhr.setRequestHeader('Content-Type', 'application/json');
			},
			data: JSON.stringify(obj),
			success: function (response) {
				SuccessAlert();
				console.log(response);
				//$('#put_res').text(response.message);
			},
			error: function (response) {
				ErrorAlert("");
				console.log(response);
			},
		});
	});





	$('#submit_new_Question').click(function () {
		// click
		if ($('#new_QTitle').val().trim().length == 0 || $('#CampId').val().trim().length == 0) {
			ErrorAlert();
			return;
		}
		var obj = {
			CampID: $('#CampId').val(),
			Title: $('#new_QTitle').val(),
			Q_Order: $('#new_QOrder').val(),
			Text_az: $('#new_QText_az').val(),
			Text_en: $('#new_QText_en').val(),
			Text_ru: $('#new_QText_ru').val(),
			IsEnabled: ($('#new_QIsEnabled').is(":checked")) ? 1 : 0,
			Note: $('#new_txtNote').val()
		};

		$.ajax({
			method: 'POST',
			url: 'http://192.168.111.64:8080/api/campaigns/' + $('#CampId').val() + '/questions',
			dataType: 'json',
			beforeSend: function (xhr) {
				xhr.setRequestHeader('Content-Type', 'application/json');
			},
			data: JSON.stringify(obj),
			success: function (response) {
				SuccessAlert();
				//Clear form
				$('#new_QTitle').val("");
				$('#new_QOrder').val("");
				$('#new_QText_az').val("");
				$('#new_QText_en').val("");
				$('#new_QText_ru').val("");
				$('#new_txtNote').val("");
				// End Clear form
				$("#cb_campaigns").trigger("change");

			},
			error: function (response) {
				ErrorAlert("");
				console.log(response);
			},
		});
	});



	$('#submit_new_Campaign').click(function () {
		// click
		if ($('#new_CampaignName').val().trim().length == 0) {
			ErrorAlert("Please fill the required fiedls!");
			return;
		}
		var obj = {
			CampaignName: $('#new_CampaignName').val(),
			IsEnabled: ($('#new_CIsEnabled').is(":checked")) ? 1 : 0
		};

		$.ajax({
			method: 'POST',
			url: 'http://192.168.111.64:8080/api/campaigns',
			dataType: 'json',
			beforeSend: function (xhr) {
				xhr.setRequestHeader('Content-Type', 'application/json');
			},
			data: JSON.stringify(obj),
			success: function (response) {
				//SuccessAlert();
				location.reload();

			},
			error: function (response) {
				//alert('This is Error message!');
				ErrorAlert("");
				console.log(response);
			},
		});
	});




	$('#submit_Answers').click(function () {
		var answerList = new Array();
		var validation = 0;
		$('[id^=answer_]').each(function (index, element) {
			//console.log( index + ": " + this.id + "=" + this.id.substring(7) );
			var itemNum = this.id.substring(7);			
			var obj = {
				ID: $('#AID_' + itemNum).val(),
				QID: $('#QID').val(),
				NextQID: $('#ANextQID_' + itemNum).val(),
				Title: $('#ATitle_' + itemNum).val(),
				Q_Order: $('#AOrder_' + itemNum).val(),
				Text_az: $('#AText_az_' + itemNum).val(),
				Text_en: $('#AText_en_' + itemNum).val(),
				Text_ru: $('#AText_ru_' + itemNum).val(),
				IsEnabled: ($('#AIsEnabled_' + itemNum).is(":checked")) ? 1 : 0
			};

			answerList.push(obj);
		});

		//    console.log(answerList);

		$.ajax({
			method: 'PUT',
			url: 'http://192.168.111.64:8080/api/questions/' + $('#QID').val() + '/answers',
			dataType: 'json',
			beforeSend: function (xhr) {
				xhr.setRequestHeader('Content-Type', 'application/json');
			},
			data: JSON.stringify(answerList),
			success: function (response) {
				// console.log(response);
				SuccessAlert();
			},
			error: function (response) {
				ErrorAlert("");
				console.log(response);
			},
		});

	});


});



function ErrorAlert(msg) {
	if (msg.trim().length == 0) {
		msg = "Something went wrong!";
	}
	swal({
		title: "Oops...",
		text: msg,
		confirmButtonColor: "#66BB6A",
		type: "error"
	});
}

function SuccessAlert() {
	swal({
		title: "Good job!",
		text: "Successfully saved!",
		confirmButtonColor: "#66BB6A",
		type: "success"
	});
}