$(function () {
	var answerCounter = 0;
    $( "#btn_addAnswer" ).click(function() {
    //alert( "Handler for btn_addAnswer.click() called." );
	answerCounter++;
    $( "#panel_Answers" ).append('<div id="answer_' + answerCounter + '" class="col-md-12" style="border: 2px solid #ddd;	border-radius: 5px; margin-bottom: 20px; margin-top: 10px; padding-bottom: 10px;">       \
    <br/><ul style="float: right;" class="fab-menu fab-menu-top" data-fab-toggle="click" style="z-index: 1002;"><li><a class="fab-menu-btn btn btn-danger btn-float btn-rounded btn-icon" onClick = "removeElement(' + answerCounter + ');"><i class="fab-icon-open icon-minus3"></i><i class="fab-icon-close icon-cross2"></i></a></li></ul> \
												<div class="col-md-12">                                                       \
												    <input type="hidden" id="AID_' + answerCounter + '" value="0" /> \
													<input type="text" class="form-control" id="ATitle_' + answerCounter + '" >        \
													<div class="label-block">                                                 \
														<span class="label label-primary">Answer title</span>                 \
													</div>                                                                    \
												</div> 											                              \
												<div class="col-md-12">                                                       \
													<input type="text" class="form-control" id="AText_az_' + answerCounter + '" >    \
													<div class="label-block">                                                 \
														<span class="label label-primary">Answer text (AZ)</span>             \
													</div>                                                                    \
												</div>                                                                        \
												<div class="col-md-12">                                                       \
													<input type="text" class="form-control" id="AText_en_' + answerCounter + '" >    \
													<div class="label-block">                                                 \
														<span class="label label-primary">Answer text (EN)</span>             \
													</div>                                                                    \
												</div>                                                                        \
												<div class="col-md-12">                                                       \
													<input type="text" class="form-control" id="AText_ru_' + answerCounter + '">    \
													<div class="label-block">                                                 \
														<span class="label label-primary">Answer text (RU)</span>             \
													</div>                                                                    \
												</div>                                                                        \
												<div class="col-md-2">                                                       \
													<input type="number" value="0"  min="0" max="100" class="form-control" id="AOrder_' + answerCounter + '" >        \
													<div class="label-block">                                                 \
														<span class="label label-primary">Order</span>                        \
													</div>                                                                    \
												</div>                                                                        \
												<div class="col-md-2">                                                       \
												<input type="checkbox" checked class="switchery-primary" id="AIsEnabled_' + answerCounter + '" >\
													<div class="label-block">                                                 \
														<span class="label label-primary">Is Enabled</span>                   \
													</div>                                                                    \
												</div>                                                                        \
												<div class="col-md-5">                                                    \
													<div class="input-group">                                                       \
														<input  type="hidden"  id="ANextQID_' + answerCounter + '" >        \
														<input  type="text" readonly="readonly" class="form-control" id="ANextQTitle_' + answerCounter + '" >        \
														<span class="input-group-btn">                                        \
															<button class="btn btn-default" onClick = "loadNextQuestions(' + answerCounter + ');" type="button">Select</button>     \
														</span>                                                               \
													</div>                                                                    \
													<div class="label-block">                                             \
														<span class="label label-primary">Next Question</span>            \
													</div>                                                                \
												</div>                                                                        \
                        </div>');

});



});


function removeElement(id) {
	var element = document.getElementById("answer_"+ id);
	element.parentNode.removeChild(element);
}




function NextQuestionDialog(options, id){
	
	bootbox.dialog({
		title: "This is a form in a modal.",
		message: '<div class="row">  ' +
					'<div class="col-md-12">' +
						'<div class="form-group">' +
							'<label class="display-block">Please Select Question</label>'+
							'<select id="cb_NextQuestion" class="select-results-color" style="width: 100%; font-size: 20px;">'+
								'<option value="0" selected="selected" disabled>Select...</option>'+
								options +
							'</select>'+
						'</div>' +
					'</div>' +
				'</div>',
		buttons: {
			success: {
				label: "Save",
				className: "btn-success",
				callback: function () {
					$('#ANextQID_'+id).val($('#cb_NextQuestion').val());
					$('#ANextQTitle_'+id).val($('#cb_NextQuestion  option:selected').text());
				}
			}
		}
	}
);

}



function loadNextQuestions(id) {
	// console.log('loadQuestions:' + CampId);
	var CampId = $('#cb_campaigns').val();
	var result = "";
    $.ajax({
        method: 'GET',
        url: 'http://192.168.111.64:8080/api/campaigns/' + CampId + '/questions',
        success: function (response) {
            //alert('Request has been sent to Server');
            console.log(response);
            var myObj, i, j, x = "";
            myObj = response;

            //console.log(myObj);
            if (myObj.code == 200) {
                for (i in myObj.data) {
                    //console.log(myObj.data[i]);
                    result += "<option value='" + myObj.data[i].id + "'>" + myObj.data[i].title + "</option>";
				}
				NextQuestionDialog(result, id);
				
            } else {
                alert('Response-Code:' + myObj.code + ', Message:' + myObj.message);
            }
        },
        error: function (response) {
            alert('Error:' + response.message);
            console.log(response);
        },
	});
	
}