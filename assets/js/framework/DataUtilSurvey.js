$(function () {

    $('#start_survey').click(function () {
        // click
        if ($('#CustomerName').val().trim().length == 0) {
            ErrorAlert("Please fill the required fiedls!");
            return;
        }

        var obj = {
            CampID: $('#cb_campaigns').val(),
            AgentID: $('#AgentID').val(),
            CustomerName: $('#CustomerName').val(),
            CIF: $('#CIF').val(),
            MSISDN: $('#MSISDN').val()
        };

        $.ajax({
            method: 'POST',
            url: 'http://192.168.111.64:8080/survey/campaigns/',
            dataType: 'json',
            beforeSend: function (xhr) {
                xhr.setRequestHeader('Content-Type', 'application/json');
            },
            data: JSON.stringify(obj),
            success: function (response) {
                //SuccessAlert();
                //console.log(response);
                var myObj, i = "";
                myObj = response;
                if (myObj.code == 200) {
                    $('#SID').val(myObj.data.id);
                    submitAndGetNextQuestion($('#cb_campaigns').val(), myObj.data.id);
                } else {
                    alert('Response-Code:' + myObj.code + ', Message:' + myObj.message);
                }

            },
            error: function (response) {
                ErrorAlert();
                console.log(response);
            },
        });
    });



    $('#submitAnswer').click(function () {
        // click
        submitAndGetNextQuestion($('#cb_campaigns').val(), $('#SID').val());
    });




});

function submitAndGetNextQuestion(CampId, paramSID) {
    var obj = {
        SID: paramSID,
        QID: $('#QID').val(),
        AID: $('input:radio[name=rbtnAnswer]:checked').val(),
        Answer: $('#txtAnswer').val()
    };
    //console.log(obj);
    $.ajax({
        method: 'POST',
        url: 'http://192.168.111.64:8080/survey/campaign/' + CampId + '/questions',
        dataType: 'json',
        beforeSend: function (xhr) {
            xhr.setRequestHeader('Content-Type', 'application/json');
        },
        data: JSON.stringify(obj),
        success: function (response) {
            //alert('Request has been sent to Server');
            //console.log(response);
            var myObj, i = "";
            myObj = response;

            //console.log(myObj);
            if (myObj.code == 200) {
                if (myObj.data != null) {
                    var questionText;
                    switch ($('#Lang').val()) {
                        case "EN":
                            questionText = myObj.data.qstn.text_en;
                            break;
                        case "RU":
                            questionText = myObj.data.qstn.text_ru;
                            break;
                        default:
                            questionText = myObj.data.qstn.text_az;
                            break;
                    }
                    $('#currentQuestionText').html("<ul><li>" + questionText + "</li></ul>");
                    $('#QID').val(myObj.data.qstn.id);
                    $('#noteText').html(myObj.data.qstn.note);
                    var answText;
                    var answhtml = "";
                    var firstansw = 1;
                    for (i in myObj.data.answ) {
                        switch ($('#Lang').val()) {
                            case "EN":
                                answText = myObj.data.answ[i].text_en;
                                break;
                            case "RU":
                                answText = myObj.data.answ[i].text_ru;
                                break;
                            default:
                                answText = myObj.data.answ[i].text_az;
                                break;
                        }
                        answhtml = answhtml + "<div class='radio'><label><input type='radio' name='rbtnAnswer' class='styled' value='" + myObj.data.answ[i].id + "' " + ((firstansw == 1)?"checked":" ") + " >" + answText + "</label></div>";
                        //$('#rbts_Answers').append("<label><input name='rbtn_Answer_" + myObj.data.answ[i].id + "' type='radio' class='styled'>" + myObj.data.answ[i].text_az + " / " + myObj.data.answ[i].text_en + " / " + myObj.data.answ[i].text_ru  + "</label>");
                        firstansw = 0;
                    }
                    $('#rbtnAnswers').html(answhtml);
                    $('#txtAnswer').val("");
                    loadSurveyHist(paramSID);
                    $('#CurrentQuestion').show();
                    $('#start_survey').hide();
                }
                else {
                    $('#CurrentQuestion').hide();
                    $('#start_survey').show();
                    $('#tbl_surveyHist').html("");
                    $('#panelSurveyHist').hide();
                    SuccessSurveyAlert();
                }
            } else {
                alert('Response-Code:' + myObj.code + ', Message:' + myObj.message);
            }
        },
        error: function (response) {
            alert('Error:' + response.message);
            console.log(response);
        },
    });
}


function loadSurveyHist(SID) {
    $.ajax({
        method: 'GET',
        url: 'http://192.168.111.64:8080/survey/' + SID,
        success: function (response) {
            //alert('Request has been sent to Server');
            console.log(response);
            var myObj, i = "";
            var count = 0;
            myObj = response;

            //console.log(myObj);
            if (myObj.code == 200) {
                $('#tbl_surveyHist').html("");
                for (i in myObj.data) {
                    count++;
                    $('#tbl_surveyHist').append("<tr><td>" + (count) + "</td><td>" + myObj.data[i].qtext_en + "</td><td>" + myObj.data[i].atext_en + "</td><td>" + myObj.data[i].answer + "</td></tr>");
                }
                if (count > 0) {
                    $('#panelSurveyHist').show();
                }
            } else {
                alert('Response-Code:' + myObj.code + ', Message:' + myObj.message);
            }
        },
        error: function (response) {
            alert('Error:' + response.message);
            console.log(response);
        },
    });
}

function ErrorAlert(msg) {
    swal({
        title: "Oops...",
        text: "Something went wrong! \n" + msg,
        confirmButtonColor: "#66BB6A",
        type: "error"
    });
}

function SuccessAlert() {
    swal({
        title: "Good job!",
        text: "Successfully saved!",
        confirmButtonColor: "#66BB6A",
        type: "success"
    });
}

function SuccessSurveyAlert() {
    swal({
        title: "Good job!",
        text: "The survey is over! \n Now go to the next one!",
        confirmButtonColor: "#66BB6A",
        type: "success"
    });
}




